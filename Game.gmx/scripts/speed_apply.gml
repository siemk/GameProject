
if (place_meeting(x+xspd,y,parent_solid)){
    while !(place_meeting(x+sign(xspd),y,parent_solid)){
        x += sign(xspd);   
    }
    xspd = 0;
}
x += xspd;
if (place_meeting(x,y+yspd,parent_solid)){
    while !(place_meeting(x,y+sign(yspd),parent_solid)){
        y += sign(yspd);   
    }
    yspd = 0;
}
y += yspd;
